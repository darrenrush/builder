<?php #Logout.php
/**
 * User: Darren
 * Date: 9/16/2015
 * Time: 12:14 AM
 */
class Logout extends CI_Controller {
    /**
     * Logout function on index
     */
    function index() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}