<?php #login_fail.php
/**
 * User: Darren
 * Date: 9/15/2015
 * Time: 11:40 PM
 */
?>
<div class="alert alert-danger" role="alert"><strong>Notice:</strong> You are not allowed to access this page.
    Please <?php echo anchor('login', 'login ') ?> first.
</div>