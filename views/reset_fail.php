<div class="alert alert-danger" role="alert"><strong>Notice:</strong> Your password could not be reset, your password
    reset link has expired.
    Try resetting your password again <?php echo anchor('login/forgot', 'password reset', 'class="alert-link"') ?>.
</div>