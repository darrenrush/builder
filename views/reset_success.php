<div class="alert alert-success" role="alert"><strong>Congrats!</strong> Your password has been
    reset <?php echo anchor('login', 'Login Now') ?>.
</div>