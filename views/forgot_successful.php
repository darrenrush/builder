<div class="alert alert-success" role="alert"><strong>A password reset email has been sent
        to <?php echo $email_address ?>. The password reset link will expire in 24-hours.</strong></div>