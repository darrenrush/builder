<?php #forgot_form.php
/**
 * User: Darren
 * Date: 9/17/2015
 * Time: 6:48 PM
 */
?>
<div id="forgot_form" class="well">
    <h1>Reset Password</h1>
    <?php echo form_open('login/forgot_password'); ?>


    <?php $data_name['email_address'] = array('name' => 'email_address', 'value' => set_value('email_address'), 'placeholder' => 'Email Address', 'required' => '', 'class' => 'form-control'); ?>
    <div class="form-group">
        <?php echo form_input($data_name['email_address']); ?>
    </div>
    <?php echo form_submit('submit', 'Request', 'class="btn btn-primary"'); ?>
</div>
<div class="validation_errors">
    <?php echo validation_errors(''); ?>
</div>