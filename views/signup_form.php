<div id="create_account" class="well">
    <h1>Create an Account</h1>
    <fieldset>
        <legend>Personal Information</legend>
        <?php echo form_open('login/create_member'); ?>
        <?php $data_name['first_name'] = array('name' => 'first_name', 'value' => set_value('first_name'), 'placeholder' => 'First Name', 'required' => '', 'class' => 'form-control'); ?>
        <?php $data_name['last_name'] = array('name' => 'last_name', 'value' => set_value('last_name'), 'placeholder' => 'Last Name', 'required' => '', 'class' => 'form-control'); ?>
        <?php $data_name['email_address'] = array('name' => 'email_address', 'value' => set_value('email_address'), 'placeholder' => 'Email Address', 'required' => '', 'class' => 'form-control'); ?>
        <div class="form-group">
            <?php echo form_input($data_name['first_name']); ?>
        </div>
        <div class="form-group">
            <?php echo form_input($data_name['last_name']); ?>
        </div>
        <div class="form-group">
            <?php echo form_input($data_name['email_address']); ?>
        </div>
    </fieldset>
    <br/>
    <fieldset>
        <legend>Login Info</legend>
        <?php $data_name['password'] = array('name' => 'password', 'placeholder' => 'Password', 'required' => '', 'class' => 'form-control'); ?>
        <?php $data_name['password2'] = array('name' => 'password2', 'placeholder' => 'Password Confirm', 'required' => '', 'class' => 'form-control'); ?>
        <div class="form-group">
            <?php echo form_password($data_name['password']); ?>
        </div>
        <div class="form-group">
            <?php echo form_password($data_name['password2']); ?>
        </div>
        <?php echo form_submit('submit', 'Create Account', 'class="btn btn-primary btn-large"'); ?>
    </fieldset>
</div>
<?php if (isset($user_exists)): ?>
    <div class="validation_errors">
        <div role="alert" class="alert alert-danger">
            <span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span>
            <span class="sr-only">Error:</span>
            The username you have chosen already exists.
        </div>
    </div>
<?php endif; ?>

<?php if (isset($email_exists)): ?>
    <div class="validation_errors">
        <div role="alert" class="alert alert-danger">
            <span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span>
            <span class="sr-only">Error:</span>
            The email address you have chosen already exists.
        </div>
    </div>
<?php endif; ?>
<div class="validation_errors">
    <?php echo validation_errors(''); ?>
</div>