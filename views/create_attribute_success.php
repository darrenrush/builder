<?php #create_attribute_success.php
/**
 * User: Darren
 * Date: 9/26/2015
 * Time: 10:04 PM
 */
?>
<div class="alert alert-success" role="alert"><strong>Congrats!</strong> Attribute has been successfully
    created. <?php echo anchor('view/create_attribute', 'Create another') ?>.
</div>
