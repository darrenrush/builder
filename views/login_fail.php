<?php #login_fail.php
/**
 * User: Darren
 * Date: 9/16/2015
 * Time: 9:04 PM
 */
?>
<div id="login_fail">
    <div role="alert" class="alert alert-danger">
        <span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span>
        <span class="sr-only">Error:</span>
        There was an error with your E-Mail / Password combination. Please try again.
    </div>
</div>