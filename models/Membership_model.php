<?php #membership_model.php
/**
 * User: Darren
 * Date: 9/15/2015
 * Time: 9:31 PM
 */
class Membership_model extends CI_Model {

    /**
     * @return mixed
     * Validate the user on login, return that user's information from the database.
     */
    function validate() {
        $password = $this->input->post('password'); //the password the user typed
        $this->db->where('membership.email_address', $this->input->post('email_address')); // get rows where username = typed email
        $query = $this->db->get('membership'); //query it on the membership table
        if ($query->num_rows() == 1) { // if username exists...
            $row = $query->row_array(); // get the returned row
            $db_password = $row['password']; // get the password from the returned row
            if (password_verify($password, $db_password)) // verify the password is correct
                return $row; // return to the controller $row if it matches
        }
    }

    /**
     * @param $type
     * @return mixed
     *
     * Get type of either member or client, members set their password, clients have their password set for them.
     */
    function insert_member($type) {
        $options = $this->get_hash_cost_option(); // cost of hashing on the server
        if ('member' == $type)
            $password = $this->input->post('password'); // the value the user typed in the field
        elseif ('client' == $type)
            $password = $this->create_token(); // the value the user typed in the field
        $hash = password_hash($password, PASSWORD_BCRYPT, $options); // hash the password they typed
        $data = array('membership.first_name' => $this->input->post('first_name'), 'membership.last_name' => $this->input->post('last_name'), 'membership.password' => $hash, 'membership.email_address' => $this->input->post('email_address'));
        $insert = $this->db->insert('membership', $data);

        return $insert;
    }

    /**
     * @return array
     *
     * Returns our hash strength options
     */
    function get_hash_cost_option() {
        return $options = ['cost' => 12];
    }

    /**
     * @return string
     *
     * Returns token for generated password
     */
    function create_token() {
        $token = md5(rand() . microtime());

        return $token;
    }

    function insert_admin() {
        $options = $this->get_hash_cost_option();
        $password = $this->input->post('password');
        $hash = password_hash($password, PASSWORD_BCRYPT, $options); // hash the password they typed
        $data = array('membership.first_name' => $this->input->post('first_name'), 'membership.last_name' => $this->input->post('last_name'), 'membership.password' => $hash, 'membership.admin' => 1, 'membership.email_address' => $this->input->post('email_address'));
        $insert = $this->db->insert('membership', $data);

        return $insert;
    }

    /**
     * @param $email
     * @return bool
     * Does the email exist in the database already?
     */
    function checkemail($email) {
        $this->db->where('email_address', $email);
        $query = $this->db->get('membership');
        if ($query->num_rows() == 1) {
            return TRUE;
        }
        else return FALSE;
    }

    /**
     * @param $email
     *
     * Generates a token and sends ot a forgot password email.
     */
    function forgot_password($email) {
        $email_encoded = urlencode($email);
        $token = $this->create_token();
        $options = $this->get_hash_cost_option(); // cost of hashing on the server
        $data = array('email_recovery.email_address' => $email, 'email_recovery.token' => password_hash($token, PASSWORD_BCRYPT, $options), 'email_recovery.timestamp' => date('m-d-Y'));
        $this->db->where('email_recovery.email_address', $this->input->post('email_address'));
        $query = $this->db->get('email_recovery');
        if ($query->num_rows() == 1) {
            $this->db->where('email_recovery.email_address', $email);
            $this->db->update('email_recovery', $data);
        }
        else {
            $this->db->insert('email_recovery', $data);
        }
        $config = Array( //config our email
            'mailtype' => "html");
        $this->load->library('email', $config);
        $this->email->from('info@bussellbuilding.com', 'Bussell Building');
        $this->email->to($email);
        $this->email->subject('Bussell Building - Account Information');
        $this->email->message('<img style="max-height:80px" src="http://bussellbuilding.com/wp-content/uploads/2013/09/new-bussell-building-logo.jpg" alt="Bussell Building" /><br /><br /><b>This message is from <a href="http://www.bussellbuilding.com">Bussell Building</a></b><br /><br />In order to make your selections for your custom built home you need to login. An account has been created for you. You can set your password by ' . anchor("login/reset_password/$email_encoded/$token", 'clicking here') . '. This password link will expire within 24-hours. Once you login you will be able to make your selections.');
        if (!$this->email->send())
            echo $this->email->print_debugger();
    }

    /**
     * @param $uri_email
     * @param $uri_token
     * @return bool
     *
     * Confirm its ok to reset the password.
     */
    function reset_password_check($uri_email, $uri_token) {
        $this->db->where('email_recovery.email_address', $uri_email);
        $this->db->where('email_recovery.timestamp', date('m-d-Y'));
        $query = $this->db->get('email_recovery');
        if ($query->num_rows() == 1) {
            $this->db->where('email_recovery.email_address', $uri_email);
            $query = $this->db->get('email_recovery');
            if ($query->num_rows() == 1) {
                $row = $query->row_array();
                $db_password = $row['token'];
                if (password_verify($uri_token, $db_password)) {
                    return TRUE;
                }
            }
        }
    }

    /**
     * @param $email
     * @return bool
     *
     * Update the password for the user
     */
    function reset_password($email) {
        $options = $this->get_hash_cost_option(); // cost of hashing on the server
        $password = $this->input->post('password'); // the value the user typed in the field
        $hash = password_hash($password, PASSWORD_BCRYPT, $options); // hash the password they typed
        $data = array('password' => $hash);
        $this->db->where('membership.email_address', $email);
        $this->db->update('membership', $data);
        $this->db->delete('email_recovery', array('email_recovery.email_address' => $email));

        return TRUE;
    }

    /**
     * @param $email
     * @return mixed
     *
     * Return the membership id by email.
     */
    function get_membership_id($email) {
        $this->db->where('membership.email_address', $email);
        $query = $this->db->get('membership');
        if ($query->num_rows() == 1) {
            $row = $query->row_array();
            $membership_id = $row['membership_id'];

            return $membership_id;
        }
    }

    function get_member_email($membership_id) {
        $query = $this->db->get_where('membership', array('membership_id' => $membership_id));
        foreach ($query->result() as $row) {
            $email = $row->email_address;
        }
        if (isset($email))
            return $email;
        else return '';
    }

    function get_member_id($property_id) {
        $query = $this->db->get_where('property', array('property_id' => $property_id));
        foreach ($query->result() as $row) {
            $membership_id = $row->membership_id;
        }
        if (isset($membership_id))
            return $membership_id;
        else return FALSE;
    }

    /**
     * @return bool
     *
     * See if the user is currently logged in by session.
     */
    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (isset($is_logged_in) || $is_logged_in == TRUE) {
            return TRUE;
        }
    }

    /**
     * @return bool
     *
     * See if the user who is logged in is an admin.
     */
    function is_admin() {
        $email_address = $this->session->userdata('email_address');
        $this->db->where('membership.email_address', $email_address);
        $query = $this->db->get('membership');
        if ($query->num_rows() == 1) {
            $row = $query->row_array();
            $admin = $row['admin'];
            if ($admin == 1)
                return TRUE;
        }
    }

    function is_employee() {
        $email_address = $this->session->userdata('email_address');
        $this->db->where('membership.email_address', $email_address);
        $query = $this->db->get('membership');
        if ($query->num_rows() == 1) {
            $row = $query->row_array();
            $admin = $row['admin'];
            if ($admin == 2)
                return TRUE;
        }
    }

}