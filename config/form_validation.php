<?php #form_validation.php
/**
 * User: Darren
 * Date: 9/16/2015
 * Time: 8:35 PM
 */
$config['error_prefix'] = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>    ';
$config['error_suffix'] = '    </div>';